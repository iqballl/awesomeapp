# AwesomeApp.

Evermos - Testing Mobile Apps Engineer (Remote).

Simple Android movies app using MVVM clean architecture.
An android app built using Kotlin that consumes Pexels API to showing Data.


# Libraries.

- Koin - Dependency Injection library.

- Jetpack

    * Android KTX - Provide concise, idiomatic Kotlin to Jetpack and Android platform APIs.
    * AndroidX - Major improvement to the original Android Support Library, which is no longer maintained.
    * Lifecycle - Perform actions in response to a change in the lifecycle status of another component, such as     activities and fragments.
    * LiveData - Lifecycle-aware, meaning it respects the lifecycle of other app components, such as activities, fragments, or services.
    * ViewModel - Designed to store and manage UI-related data in a lifecycle conscious way. The ViewModel class allows data to survive configuration changes such as screen rotations.
    * Data Binding - Allows you to bind UI components in your layouts to data sources in your app using a declarative format rather than programmatically.
    * Room - Provides an abstraction layer over SQLite used for offline data caching.
    * Navigation Component-Component that allows easier implementation of navigation from simple button clicks to more complex patterns.

- Retrofit - Type-safe http client and supports coroutines out of the box.

- OkHttp-Logging-Interceptor - Logs HTTP request and response data.

- Material Design - Build awesome beautiful UIs.



