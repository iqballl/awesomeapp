package iqbal.awesomeapp.domain.repositories

import io.reactivex.Single
import iqbal.awesomeapp.domain.models.PhotoResponse
import iqbal.awesomeapp.domain.models.PhotosItem

interface RemoteRepository {
    fun fetchPhotos(page: Int): Single<PhotoResponse>
    fun fetchDetailPhoto(idPhoto: Int): Single<PhotosItem>
}