package iqbal.awesomeapp.domain.interactor

import io.reactivex.Single
import iqbal.awesomeapp.domain.models.PhotoResponse
import iqbal.awesomeapp.domain.repositories.RemoteRepository

class PhotoListUseCase(private val remoteRepository: RemoteRepository) {

    fun execute(page: Int): Single<PhotoResponse> {
        return remoteRepository.fetchPhotos(page)
    }

}