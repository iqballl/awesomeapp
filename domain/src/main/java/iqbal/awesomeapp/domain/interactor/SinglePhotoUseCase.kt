package iqbal.awesomeapp.domain.interactor

import io.reactivex.Single
import iqbal.awesomeapp.domain.models.PhotosItem
import iqbal.awesomeapp.domain.repositories.RemoteRepository

class SinglePhotoUseCase(private val remoteRepository: RemoteRepository) {
    fun execute(id: Int): Single<PhotosItem> {
        return remoteRepository.fetchDetailPhoto(id)
    }
}