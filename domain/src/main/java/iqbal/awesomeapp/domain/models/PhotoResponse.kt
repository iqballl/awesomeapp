package iqbal.awesomeapp.domain.models

data class PhotoResponse(
	var nextPage: String? = null,
	var perPage: Int? = null,
	var page: Int? = null,
	var totalResults: Int? = null,
	var photos: List<PhotosItem>? = null,
)