package iqbal.awesomeapp.domain.models

data class PhotosItem(
    var src: Src? = null,
    var width: Int? = null,
    var avgColor: String? = null,
    var alt: String? = null,
    var photographer: String? = null,
    var photographerUrl: String? = null,
    var id: Int? = null,
    var url: String? = null,
    var photographerId: Int? = null,
    var liked: Boolean? = null,
    var height: Int? = null
)