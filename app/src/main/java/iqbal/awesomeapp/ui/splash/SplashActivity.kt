package iqbal.awesomeapp.iqbal.awesomeapp.ui.splash

import android.os.Bundle
import iqbal.awesomeapp.iqbal.awesomeapp.data.BaseActivity
import iqbal.awesomeapp.iqbal.awesomeapp.data.navigation.navigateToHome

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        navigateToHome()
    }

}