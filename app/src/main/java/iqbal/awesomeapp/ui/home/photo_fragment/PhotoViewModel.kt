package iqbal.awesomeapp.iqbal.awesomeapp.ui.home.photo_fragment

import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import iqbal.awesomeapp.domain.interactor.PhotoListUseCase
import iqbal.awesomeapp.domain.models.PhotosItem
import iqbal.awesomeapp.iqbal.awesomeapp.utils.Resource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class PhotoViewModel(private val photoListUseCase: PhotoListUseCase) : ViewModel() {

    private val stateFlow = MutableStateFlow<Resource<List<PhotosItem>>>(Resource.empty())
    private var currentPage = 1
    private var lastPage = 99

    var disposable: Disposable? = null

    val popularMoviesState: StateFlow<Resource<List<PhotosItem>>>
        get() = stateFlow

    fun fetchPhotos() {
        stateFlow.value = Resource.loading()
        disposable = photoListUseCase.execute(currentPage)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ res ->
                //lastPage = res.total_pages
                res.photos?.let {
                    stateFlow.value = Resource.success(it)

                }
            }, { throwable ->
                lastPage = currentPage // prevent loading more pages
                throwable.localizedMessage?.let {
                    stateFlow.value = Resource.error(it)
                }
            })
    }

    fun fetchNextPhotos() {
        currentPage++
        fetchPhotos()
    }

    fun refreshPhotos() {
        currentPage = 1
        fetchPhotos()
    }

    fun isFirstPage(): Boolean {
        return currentPage == 1
    }

    fun isLastPage(): Boolean {
        return currentPage == lastPage
    }

}