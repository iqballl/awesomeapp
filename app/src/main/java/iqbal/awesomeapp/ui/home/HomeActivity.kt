package iqbal.awesomeapp.iqbal.awesomeapp.ui.home

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import iqbal.awesomeapp.R
import iqbal.awesomeapp.databinding.ActivityHomeBinding
import iqbal.awesomeapp.iqbal.awesomeapp.data.BaseActivity


class HomeActivity : BaseActivity() {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbarHome)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_switch_layout) {
            //switchLayout()
            //switchIcon(item)
            return true
        }
        return super.onOptionsItemSelected(item)
    }


}
