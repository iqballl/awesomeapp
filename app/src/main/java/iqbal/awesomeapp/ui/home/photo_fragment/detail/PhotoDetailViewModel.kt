package iqbal.awesomeapp.iqbal.awesomeapp.ui.home.photo_fragment.detail

import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import iqbal.awesomeapp.domain.interactor.SinglePhotoUseCase
import iqbal.awesomeapp.domain.models.PhotosItem
import iqbal.awesomeapp.iqbal.awesomeapp.utils.Resource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class PhotoDetailViewModel(
    private val singlePhotoUseCase: SinglePhotoUseCase,
) : ViewModel() {

    private val singleMovieStateFlow = MutableStateFlow<Resource<PhotosItem>>(Resource.empty())
    private val favoritesStateFlow = MutableStateFlow<Resource<Boolean>>(Resource.empty())
    var disposable: Disposable? = null

    val singleMovieState: StateFlow<Resource<PhotosItem>>
        get() = singleMovieStateFlow

    val favoritesState: StateFlow<Resource<Boolean>>
        get() = favoritesStateFlow

    fun fetchSinglePhoto(id: Int) {
        singleMovieStateFlow.value = Resource.loading()

        disposable = singlePhotoUseCase.execute(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ res ->
                singleMovieStateFlow.value = Resource.success(res)
            }, { throwable ->
                throwable.localizedMessage?.let {
                    singleMovieStateFlow.value = Resource.error(it)
                }
            })
    }

}