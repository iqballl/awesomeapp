package iqbal.awesomeapp.iqbal.awesomeapp.ui.home.photo_fragment.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import iqbal.awesomeapp.R
import iqbal.awesomeapp.data.utils.dp
import iqbal.awesomeapp.databinding.ItemPhotoListBinding
import iqbal.awesomeapp.domain.models.PhotosItem
import iqbal.awesomeapp.iqbal.awesomeapp.data.glide.load

class PhotoListAdapter(val context: Context?, var items: List<PhotosItem> = ArrayList()) :
    RecyclerView.Adapter<ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(
            data: PhotosItem,
            container: View
        ) // pass ImageView to make shared transition
    }

    private var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemPhotoListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val singleDataPhoto = items[position]
        holder.tvTitlePhotographerName.text = "Photographer Name"
        holder.tvPhotographerName.text = "- ${singleDataPhoto.photographer} -"

        holder.ivMoviePoster.setImageResource(R.drawable.placeholder_image)
        holder.ivMoviePoster.transitionName = singleDataPhoto.id.toString()
        holder.llMovieTextContainer.setBackgroundColor(Color.DKGRAY)

        holder.ivMoviePoster.load(
            url = singleDataPhoto.src?.medium.toString(),
            crossFade = true, width = 160.dp, height = 160.dp
        ) { color ->
            holder.llMovieTextContainer.setBackgroundColor(ContextCompat.getColor(context!!, R.color.ic_launcher_background))
        }

        holder.cvMovieContainer.setOnClickListener {
            onItemClickListener?.onItemClick(singleDataPhoto, holder.ivMoviePoster)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setOnMovieClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    fun fillList(items: List<PhotosItem>) {
        this.items += items
        notifyDataSetChanged()
    }

    fun clear() {
        this.items = emptyList()
    }

    fun getRating(movie: PhotosItem): String {
        /*return if (movie.vote_count == 0 && context != null) {
            context.getString(R.string.no_ratings)
        } else {
            val starIcon = 9733.toChar()
            "${movie.vote_average} $starIcon"
        }*/
        return ""
    }
}

class ViewHolder(binding: ItemPhotoListBinding) : RecyclerView.ViewHolder(binding.root) {
    val cvMovieContainer: CardView = binding.cvMovieContainer
    val llMovieTextContainer: LinearLayout = binding.llTextContainer
    val tvTitlePhotographerName: TextView = binding.tvTitlePhotographerName
    val tvPhotographerName: TextView = binding.tvPhotographerName
    val ivMoviePoster: ImageView = binding.ivPhoto
}