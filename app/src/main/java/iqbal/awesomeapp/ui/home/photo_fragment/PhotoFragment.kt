package iqbal.awesomeapp.iqbal.awesomeapp.ui.home.photo_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.ActivityNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.marcoscg.movies.common.recyclerview.PaginationScrollListener
import iqbal.awesomeapp.R
import iqbal.awesomeapp.data.utils.gone
import iqbal.awesomeapp.data.utils.visible
import iqbal.awesomeapp.databinding.FragmentPhotoListBinding
import iqbal.awesomeapp.domain.models.PhotosItem
import iqbal.awesomeapp.iqbal.awesomeapp.ui.home.photo_fragment.adapter.PhotoListAdapter
import iqbal.awesomeapp.iqbal.awesomeapp.utils.Resource
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import timber.log.Timber
import androidx.core.util.Pair as UtilPair

class PhotoFragment : Fragment(R.layout.fragment_photo_list), PhotoListAdapter.OnItemClickListener {

    private val popularViewModel: PhotoViewModel by sharedViewModel()
    private val movieListAdapter: PhotoListAdapter by inject()

    private var _binding: FragmentPhotoListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPhotoListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupRecyclerView()
        setupSwipeRefresh()

        popularViewModel.refreshPhotos()
        viewLifecycleOwner.lifecycleScope.launch {
            popularViewModel.popularMoviesState.collect {
                handleMoviesDataState(it)
            }
        }
    }

    override fun onItemClick(data: PhotosItem, container: View) {
        val action = data.id?.let {
            PhotoFragmentDirections.actionListToDetailActivity(
                id = it,
            )
        }
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            requireActivity(),
            UtilPair.create(container, container.transitionName)
        )

        if (action != null) {
            findNavController().navigate(action, ActivityNavigatorExtras(options))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        popularViewModel.disposable?.dispose()
    }

    private fun handleMoviesDataState(state: Resource<List<PhotosItem>>) {
        when (state.status) {
            Resource.Status.LOADING -> {
                binding.srlFragmentMovieList.isRefreshing = true
            }
            Resource.Status.SUCCESS -> {
                binding.srlFragmentMovieList.isRefreshing = false
                loadMovies(state.data)
            }
            Resource.Status.ERROR -> {
                binding.srlFragmentMovieList.isRefreshing = false
                binding.pbFragmentMovieList.gone()
                Snackbar.make(
                    binding.srlFragmentMovieList,
                    getString(R.string.error_message_pattern, state.message),
                    Snackbar.LENGTH_LONG
                ).show()
            }
            Resource.Status.EMPTY -> {
                Timber.d("Empty state.")
            }
        }
    }

    private fun loadMovies(movies: List<PhotosItem>?) {
        movies?.let {
            if (popularViewModel.isFirstPage()) {
                movieListAdapter.clear()
            }
            movieListAdapter.fillList(it)
        }
    }

    private fun setupRecyclerView() {
        movieListAdapter.setOnMovieClickListener(this)
        binding.rvFragmentMovieList.adapter = movieListAdapter
        binding.rvFragmentMovieList.addOnScrollListener(object :
            PaginationScrollListener(binding.rvFragmentMovieList.linearLayoutManager) {
            override fun isLoading(): Boolean {
                val isLoading = binding.srlFragmentMovieList.isRefreshing
                if (isLoading) {
                    binding.pbFragmentMovieList.visible()
                } else {
                    binding.pbFragmentMovieList.gone()
                }
                return isLoading
            }

            override fun isLastPage(): Boolean {
                return popularViewModel.isLastPage()
            }

            override fun loadMoreItems() {
                popularViewModel.fetchNextPhotos()
            }
        })
    }

    private fun setupSwipeRefresh() {
        binding.srlFragmentMovieList.setOnRefreshListener {
            popularViewModel.refreshPhotos()
        }
    }
}