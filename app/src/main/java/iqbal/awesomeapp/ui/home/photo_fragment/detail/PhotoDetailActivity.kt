package iqbal.awesomeapp.iqbal.awesomeapp.ui.home.photo_fragment.detail

import android.os.Bundle
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.ActivityNavigator
import androidx.navigation.navArgs
import iqbal.awesomeapp.R
import iqbal.awesomeapp.data.utils.ColorUtils.darken
import iqbal.awesomeapp.data.utils.dp
import iqbal.awesomeapp.data.utils.gone
import iqbal.awesomeapp.data.utils.visible
import iqbal.awesomeapp.databinding.ActivityDetailPhotoBinding
import iqbal.awesomeapp.domain.models.PhotosItem
import iqbal.awesomeapp.iqbal.awesomeapp.data.BaseActivity
import iqbal.awesomeapp.iqbal.awesomeapp.data.glide.load
import iqbal.awesomeapp.iqbal.awesomeapp.utils.Resource
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class PhotoDetailActivity : BaseActivity() {

    private val movieDetailsViewModel: PhotoDetailViewModel by viewModel()

    private lateinit var binding: ActivityDetailPhotoBinding
    private val args: PhotoDetailActivityArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailPhotoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupToolbar()
        clearStatusBar()

        movieDetailsViewModel.fetchSinglePhoto(args.id)
        lifecycleScope.launch {
            movieDetailsViewModel.singleMovieState.collect {
                handleSingleMovieDataState(it)
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        hideFab()
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }

    private fun handleSingleMovieDataState(state: Resource<PhotosItem>) {
        when (state.status) {
            Resource.Status.LOADING -> {
                binding.progressBar.visible()
            }
            Resource.Status.SUCCESS -> {
                binding.progressBar.gone()
                loadMovieData(state.data)
            }
            Resource.Status.ERROR -> {
                binding.progressBar.gone()
                Toast.makeText(this, "Error: ${state.message}", Toast.LENGTH_LONG).show()
                finish()
            }
        }
    }

    private fun loadMovieData(data: PhotosItem?) {
        data?.let {
            binding.collapsingToolbar.title = data.photographer
            binding.detailDescription.text = data.alt
            setupPosterImage(it.src?.medium.toString())
        }
    }


    private fun setupToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        binding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun setupPosterImage(photographerUrl: String) {
        postponeEnterTransition()

        binding.ivActivityMovieDetails.transitionName = args.id.toString()
        binding.ivActivityMovieDetails.load(
            url = photographerUrl,
            width = 160.dp,
            height = 160.dp
        ) { color ->

            val mColor = ContextCompat.getColor(this, R.color.ic_launcher_background)
            window?.statusBarColor = mColor.darken
            binding.collapsingToolbar.setBackgroundColor(mColor)
            binding.collapsingToolbar.setContentScrimColor(mColor)
            startPostponedEnterTransition()
        }
    }

    // Source: https://stackoverflow.com/a/49824144
    private fun hideFab() {
        (binding.favoriteFab.layoutParams as CoordinatorLayout.LayoutParams).behavior = null
        binding.favoriteFab.requestLayout()
        binding.favoriteFab.gone()
    }

}