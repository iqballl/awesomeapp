package iqbal.awesomeapp.iqbal.awesomeapp.data.navigation


import android.app.Activity
import android.content.Intent
import iqbal.awesomeapp.iqbal.awesomeapp.ui.home.HomeActivity

fun Activity.navigateToHome() {
    this.startActivity(Intent(this, HomeActivity::class.java))
    this.finish()
}