package iqbal.awesomeapp.iqbal.awesomeapp.data

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import iqbal.awesomeapp.data.utils.WindowUtils.setToolbarTopPadding
import iqbal.awesomeapp.data.utils.WindowUtils.setTransparentStatusBar
import iqbal.awesomeapp.data.utils.WindowUtils.setupDarkTheme
import iqbal.awesomeapp.data.utils.WindowUtils.setupLightTheme
import iqbal.awesomeapp.data.utils.WindowUtils.clearStatusBar

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTransparentStatusBar(this)
        applyTheme()
    }

    override fun setSupportActionBar(toolbar: Toolbar?) {
        super.setSupportActionBar(toolbar)

        toolbar?.let {
            setToolbarTopPadding(toolbar)
        }
    }

    fun clearStatusBar() {
        clearStatusBar(this)
    }

    private fun applyTheme() {
        when(resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_YES -> {
                setupDarkTheme(this)
            }
            else -> {
                setupLightTheme(this)
            }
        }
    }
}