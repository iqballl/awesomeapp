package iqbal.awesomeapp.di

import iqbal.awesomeapp.data.repository.RemoteRepositoryImpl
import iqbal.awesomeapp.data.repository.mapper.PhotoRemoteMapper
import iqbal.awesomeapp.domain.interactor.PhotoListUseCase
import iqbal.awesomeapp.domain.interactor.SinglePhotoUseCase
import iqbal.awesomeapp.domain.repositories.RemoteRepository
import iqbal.awesomeapp.iqbal.awesomeapp.ui.home.photo_fragment.PhotoViewModel
import iqbal.awesomeapp.iqbal.awesomeapp.ui.home.photo_fragment.adapter.PhotoListAdapter
import iqbal.awesomeapp.iqbal.awesomeapp.ui.home.photo_fragment.detail.PhotoDetailViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    single { PhotoRemoteMapper() }
    factory<RemoteRepository> { RemoteRepositoryImpl(get(), get(), get()) }
    factory { PhotoListAdapter(androidContext()) }
}

val photoModule = module {
    factory { PhotoListUseCase(get()) }
    viewModel { PhotoViewModel(get()) }
}

val detailPhotoModule = module {
    factory { SinglePhotoUseCase(get()) }
    viewModel { PhotoDetailViewModel(get()) }
}
