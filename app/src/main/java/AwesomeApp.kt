package iqbal.awesomeapp

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import androidx.appcompat.app.AppCompatDelegate
import com.jakewharton.threetenabp.AndroidThreeTen
import iqbal.awesomeapp.data.local.di.localDatabaseModule
import iqbal.awesomeapp.data.remote.di.networkModule
import iqbal.awesomeapp.di.detailPhotoModule
import iqbal.awesomeapp.di.mainModule
import iqbal.awesomeapp.di.photoModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class AwesomeApp : Application() {

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        startKoin {
            androidContext(this@AwesomeApp)
            modules(
                networkModule,
                localDatabaseModule,
                mainModule,
                photoModule,
                detailPhotoModule,
            )
        }

        setupTimberLog()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    private fun setupTimberLog() {
        Timber.plant(object : Timber.DebugTree() {
            override fun createStackElementTag(element: StackTraceElement): String {
                return String.format(
                    "%s::%s:%s",
                    super.createStackElementTag(element),
                    element.methodName,
                    element.lineNumber
                )
            }
        })
    }
}