package iqbal.awesomeapp.data.repository.mapper

import iqbal.awesomeapp.data.remote.model.RemotePhotoResponse
import iqbal.awesomeapp.domain.models.PhotoResponse
import iqbal.awesomeapp.domain.models.PhotosItem
import iqbal.awesomeapp.domain.models.Src

class PhotoRemoteMapper {
    fun mapFromRemote(remotePhotoResponse: RemotePhotoResponse): PhotoResponse {
        return PhotoResponse(remotePhotoResponse.nextPage,
            remotePhotoResponse.perPage,
            remotePhotoResponse.page,
            remotePhotoResponse.totalResults,
            remotePhotoResponse.photos?.map { remotePhotos ->
                val srcData = Src(
                    remotePhotos?.src?.small,
                    remotePhotos?.src?.original,
                    remotePhotos?.src?.large,
                    remotePhotos?.src?.tiny,
                    remotePhotos?.src?.medium,
                    remotePhotos?.src?.large2x,
                    remotePhotos?.src?.portrait,
                    remotePhotos?.src?.landscape,
                )
                PhotosItem(
                    srcData,
                    remotePhotos?.width,
                    remotePhotos?.avgColor,
                    remotePhotos?.alt,
                    remotePhotos?.photographer,
                    remotePhotos?.photographerUrl,
                    remotePhotos?.id,
                    remotePhotos?.photographerUrl,
                    remotePhotos?.photographerId,
                    remotePhotos?.liked,
                    remotePhotos?.height
                )
            })
    }

    fun mapDetailFromRemote(singlePhotoData: iqbal.awesomeapp.data.remote.model.PhotosItem): PhotosItem {
        val srcData = Src(
            singlePhotoData.src?.small,
            singlePhotoData.src?.original,
            singlePhotoData.src?.large,
            singlePhotoData.src?.tiny,
            singlePhotoData.src?.medium,
            singlePhotoData.src?.large2x,
            singlePhotoData.src?.portrait,
            singlePhotoData.src?.landscape,
        )

        return PhotosItem(
            srcData,
            singlePhotoData.width,
            singlePhotoData.avgColor,
            singlePhotoData.alt,
            singlePhotoData.photographer,
            singlePhotoData.photographerUrl,
            singlePhotoData.id,
            singlePhotoData.photographerUrl,
            singlePhotoData.photographerId,
            singlePhotoData.liked,
            singlePhotoData.height
        )
    }

}