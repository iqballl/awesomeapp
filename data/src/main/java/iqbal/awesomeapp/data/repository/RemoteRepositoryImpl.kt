package iqbal.awesomeapp.data.repository

import io.reactivex.Single
import iqbal.awesomeapp.data.BuildConfig
import iqbal.awesomeapp.data.local.dao.PhotoDao
import iqbal.awesomeapp.data.remote.service.RemoteApiService
import iqbal.awesomeapp.data.repository.mapper.PhotoRemoteMapper
import iqbal.awesomeapp.domain.models.PhotoResponse
import iqbal.awesomeapp.domain.models.PhotosItem
import iqbal.awesomeapp.domain.repositories.RemoteRepository

class RemoteRepositoryImpl(
    private val apiService: RemoteApiService,
    private val photoRemoteMapper: PhotoRemoteMapper,
    private val photoDao: PhotoDao
) : RemoteRepository {

    override fun fetchPhotos(page: Int): Single<PhotoResponse> {
        //handleCache
        //val localPhoto = photoDao.getPhotos()
        return apiService.getPhotos(BuildConfig.PEXELS_API_KEY, page).map {
            photoRemoteMapper.mapFromRemote(it)
        }
    }

    override fun fetchDetailPhoto(idPhoto: Int): Single<PhotosItem> {
        return apiService.getDetailPhoto(BuildConfig.PEXELS_API_KEY, idPhoto).map {
            photoRemoteMapper.mapDetailFromRemote(it)
        }
    }


}