package iqbal.awesomeapp.data.repository.mapper

import iqbal.awesomeapp.data.local.dao.PhotosEntity
import iqbal.awesomeapp.domain.models.PhotosItem

fun PhotosEntity.toDomain(): PhotosItem {
    return PhotosItem(
        width = width,
        avgColor = avgColor,
        alt = alt,
        photographer = photographer,
        photographerUrl = photographerUrl,
        id = id,
        url = url,
        photographerId = photographerId,
        liked = liked,
        height = height,
    )
}