package iqbal.awesomeapp.data.remote.model

import com.google.gson.annotations.SerializedName

data class RemotePhotoResponse(

	@field:SerializedName("next_page")
	var nextPage: String? = null,

	@field:SerializedName("per_page")
	var perPage: Int? = null,

	@field:SerializedName("page")
	var page: Int? = null,

	@field:SerializedName("photos")
	var photos: List<PhotosItem?>? = null,

	@field:SerializedName("total_results")
	var totalResults: Int? = null
)

data class Src(

	@field:SerializedName("small")
	var small: String? = null,

	@field:SerializedName("original")
	var original: String? = null,

	@field:SerializedName("large")
	var large: String? = null,

	@field:SerializedName("tiny")
	var tiny: String? = null,

	@field:SerializedName("medium")
	var medium: String? = null,

	@field:SerializedName("large2x")
	var large2x: String? = null,

	@field:SerializedName("portrait")
	var portrait: String? = null,

	@field:SerializedName("landscape")
	var landscape: String? = null
)

data class PhotosItem(

	@field:SerializedName("src")
	var src: Src? = null,

	@field:SerializedName("width")
	var width: Int? = null,

	@field:SerializedName("avg_color")
	var avgColor: String? = null,

	@field:SerializedName("alt")
	var alt: String? = null,

	@field:SerializedName("photographer")
	var photographer: String? = null,

	@field:SerializedName("photographer_url")
	var photographerUrl: String? = null,

	@field:SerializedName("id")
	var id: Int? = null,

	@field:SerializedName("url")
	var url: String? = null,

	@field:SerializedName("photographer_id")
	var photographerId: Int? = null,

	@field:SerializedName("liked")
	var liked: Boolean? = null,

	@field:SerializedName("height")
	var height: Int? = null
)
