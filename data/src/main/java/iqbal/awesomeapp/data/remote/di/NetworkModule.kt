package iqbal.awesomeapp.data.remote.di

import android.content.Context
import android.net.ConnectivityManager
import iqbal.awesomeapp.data.remote.network.ApiEndpoint.BASE_URL
import iqbal.awesomeapp.data.remote.service.RemoteApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

fun networkConnected(context: Context): Boolean? {
    var isConnected: Boolean? = false
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = connectivityManager.activeNetworkInfo
    if (activeNetwork != null && activeNetwork.isConnected) {
        isConnected = true
    }
    return isConnected
}

val networkModule = module {

    single {
        val cacheSize = (5 * 1024 * 1024).toLong()
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

        val client = OkHttpClient.Builder()
            .retryOnConnectionFailure(false)
            .connectTimeout(1, TimeUnit.MINUTES) // menit * 60000
            .readTimeout(30, TimeUnit.SECONDS) // menit * 60000
            .writeTimeout(30, TimeUnit.SECONDS)
            // Specify the cache we created earlier.
            .cache(null)
            // Add an Interceptor to the OkHttpClient.
            //.addInterceptor(ApiHeader())
            .addInterceptor(logging)
            .build()
//        val client = OkHttpClient.Builder()
//            .cache(Cache(androidContext().cacheDir, cacheSize))
//            //.addInterceptor(ApiHeader())
//            .connectTimeout(1, TimeUnit.MINUTES) // menit * 60000
//            .readTimeout(30, TimeUnit.SECONDS) // menit * 60000
//            .writeTimeout(30, TimeUnit.SECONDS)
//            .addInterceptor(logging)
//            .addInterceptor { chain ->
//                var request = chain.request()
//                request = if (networkConnected(androidContext())!!) {
//                    request.newBuilder().header("Cache-control", "public,max-age=" + 5).build()
//                } else {
//                    request.newBuilder().header(
//                        "Cache-control",
//                        "public, only-if-cached,max-stale" + 60 * 60 * 24 * 7
//                    ).build()
//                }
//                chain.proceed(request)
//            }
//            .build()
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(RemoteApiService::class.java)
    }
}
