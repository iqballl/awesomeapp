package iqbal.awesomeapp.data.remote.service

import io.reactivex.Single
import iqbal.awesomeapp.data.remote.model.PhotosItem
import iqbal.awesomeapp.data.remote.model.RemotePhotoResponse
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface RemoteApiService {

    @GET("curated")
    fun getPhotos(
        @Header("Authorization") credentials: String,
        @Query("page") page: Int
    ): Single<RemotePhotoResponse>

    @GET("photos/{id}")
    fun getDetailPhoto(
        @Header("Authorization") credentials: String,
        @Path("id") id: Int
    ): Single<PhotosItem>
}