package iqbal.awesomeapp.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import iqbal.awesomeapp.data.local.dao.PhotoDao
import iqbal.awesomeapp.data.local.dao.PhotosEntity

@Database(
    entities = [PhotosEntity::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase(){
    abstract fun photoDao(): PhotoDao
}