package iqbal.awesomeapp.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addPhoto(data: PhotosEntity)

    @Query("SELECT * FROM photo")
    fun getPhotos(): List<PhotosEntity>

/*    @Query("SELECT * FROM upcoming_movies_table")
    suspend fun getUpcomingMovies(): List<UpcomingMovieEntity>

    @Query("SELECT * FROM now_playing_movies_table")
    suspend fun getNowPlayingMovies(): List<NowPlayingMovieEntity>

    @Query("SELECT * FROM popular_movies_table")
    suspend fun getPopularMovies(): List<UpcomingMovieEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUpcomingMovies(movies: UpcomingMovieEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addNowPlayingMovie(movies: NowPlayingMovieEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addPopularMovies(movies: PopularMovieEntity)*/
}
