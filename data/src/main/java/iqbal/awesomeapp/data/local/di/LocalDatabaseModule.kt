package iqbal.awesomeapp.data.local.di

import androidx.room.Room
import iqbal.awesomeapp.data.local.db.AppDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val localDatabaseModule = module {
    single {
        Room.databaseBuilder(
            androidApplication(),
            AppDatabase::class.java,
            "awesome.db"
        )
            .fallbackToDestructiveMigration()
            .build()
    }
    single { get<AppDatabase>().photoDao() }
    //single { get<AppDatabase>().reviewsDao() }
}