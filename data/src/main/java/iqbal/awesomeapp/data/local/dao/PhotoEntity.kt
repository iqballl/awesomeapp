package iqbal.awesomeapp.data.local.dao

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "photo")
data class PhotosEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id: Int,

    @ColumnInfo(name = "width")
    var width: Int?,

    @ColumnInfo(name = "avg_color")
    var avgColor: String?,

    @ColumnInfo(name = "alt")
    var alt: String?,

    @ColumnInfo(name = "photographer")
    var photographer: String?,

    @ColumnInfo(name = "photographer_url")
    var photographerUrl: String?,

    @ColumnInfo(name = "url")
    var url: String?,

    @ColumnInfo(name = "photographer_id")
    var photographerId: Int?,

    @ColumnInfo(name = "liked")
    var liked: Boolean?,

    @ColumnInfo(name = "height")
    var height: Int?
)